import("stdfaust.lib");

phasor(length) = %(max(length,1)) ~_ +(1); //max to avoid division by 0
latch(sig, length) = phasor(length) : ba.latch(sig);

//write sah values in a table
idx(sig,pos) = rwtable(length,0.0,writeIndex,what,readIndex)
with{
  length = 48000*20;
  what = latch(sig, length);
  writeIndex = int(phasor(length));
  readIndex = int(max(no.noise*pos, pos)); //workaround to avoid optimisation to 0f
};

//read at zero crossings
read(sig,pos,dur) = rwtable(length,0.0,writeIndex,sig,readIndex)
with{
  length = 48000*20;
  writeIndex = int(phasor(length));
  readIndex = int((start + phasor((length+(end-start))%length)) % length)
  with{
    start = idx(sig,pos);
    end = idx(sig,(pos+dur)%length);
  };
};

process(sig, pos, dur) = read(sig, pos, dur);
